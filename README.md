# nicole-learning-js

Repositório de aprendizado da Nicole Ojeda aprendendo a programar com JavaScript.

<<<<<<< HEAD
## Exercícios
=======
## Exercicios
>>>>>>> e17ca5b9c2f86274087baa00dbced2661bae66af

### 1. Salário e Gasto

Pedir pro usuário dizer o valor do salário e do gasto usando `prompt()`, e imprimir esse valor na tela com `document.write()`.

Exercício para aprendizagem de variáveis e funções.

### 2. Tabuada

Peça um número pro usuário, e escreva a tabuada desse numero pra ele, por exemplo, se o número for 2, imprima da seguinte forma:

```
2
4
6
```

E assim por diante até chegar a 10.

Exercício para aprendizagem de variáveis e funções.

### 3.Tabuada Bonitinha

Peça um número pro usuário, e escreva a tabuada desse numero pra ele, por exemplo, se o numero for 2, escreva da seguinte forma:

```
1 x 2 = 2
2 x 2 = 4
2 x 3 = 6
```

E assim por diante até chegar a 10.

Exercício para aprendizagem de interpolação e concatenação de strings.
